<!DOCTYPE html>
<html>
  <head>
    <title>Record and Upload Audio</title>
    <!-- Import wavesurfer.js -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/wavesurfer.js/4.0.0/wavesurfer.min.js"></script>
    <style>
      #waveform {
        height: 150px;
      }
    </style>
  </head>
  <body>
    <h1>Record and Upload Audio</h1>
    <div id="waveform"></div>
    <button id="record">Record</button>
    <button id="stop">Stop</button>
    <br />
    <button id="upload">Upload to S3</button>

    <!-- Import AWS SDK -->
    <script src="https://sdk.amazonaws.com/js/aws-sdk-2.956.0.min.js"></script>

    <script>
      // Initialize AWS SDK
      AWS.config.update({
        accessKeyId: 'YOUR_ACCESS_KEY_ID',
        secretAccessKey: 'YOUR_SECRET_ACCESS_KEY',
        region: 'YOUR_AWS_REGION'
      });

      // Initialize WaveSurfer
      const waveform = WaveSurfer.create({
        container: '#waveform',
        waveColor: 'black',
        progressColor: 'red'
      });

      // Initialize WebRTC recorder
      let mediaRecorder;
      let recordedChunks = [];
      const startRecording = async () => {
        const stream = await navigator.mediaDevices.getUserMedia({ audio: true });
        mediaRecorder = new MediaRecorder(stream);
        mediaRecorder.addEventListener('dataavailable', e => {
          recordedChunks.push(e.data);
        });
        mediaRecorder.start();
        waveform.empty();
        waveform.recording = true;
      };
      const stopRecording = () => {
        mediaRecorder.stop();
        waveform.recording = false;
      };

      // Record button click handler
      const recordButton = document.querySelector('#record');
      recordButton.addEventListener('click', startRecording);

      // Stop button click handler
      const stopButton = document.querySelector('#stop');
      stopButton.addEventListener('click', stopRecording);

      // Upload button click handler
      const uploadButton = document.querySelector('#upload');
      uploadButton.addEventListener('click', async () => {
        // Create S3 bucket object
        const s3 = new AWS.S3();

        // Create S3 object key
        const key = `audio-${Date.now()}.webm`;

        // Convert recorded chunks to Blob object
        const blob = new Blob(recordedChunks, { type: 'audio/webm' });

        // Upload file to S3 bucket
        const params = {
          Bucket: 'YOUR_BUCKET_NAME',
          Key: key,
          Body: blob
        };
        const response = await s3.upload(params).promise();

        // Log successful upload
        console.log(`File uploaded successfully: ${response.Location}`);
      });

      // Update waveform with recorded audio data
      mediaRecorder.addEventListener('stop', () => {
        const blob = new Blob(recordedChunks, { type: 'audio/webm' });
        const url = URL.createObjectURL(blob);
        waveform.load(url);
        recordedChunks = [];
      });
    </script>
  </body>
</html>
